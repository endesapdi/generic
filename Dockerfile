FROM opensuse/leap:15.2

MAINTAINER PDI

RUN zypper up -y \
    && zypper -q --non-interactive install curl tar gzip \
    && zypper clean -a
